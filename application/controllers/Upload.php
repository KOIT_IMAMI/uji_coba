<?php
class Upload extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('upload_model');
		$this->load->helper('download');
	}

	function index(){
		$data['data_siswa'] = $this->M_apps->tampil_data('tb_siswa_lengkap_aktif');
		$data['data_upload'] = $this->M_apps->tampil_data('m_upload');
		$this->load->view('upload/view',$data);
	}

	public function ambil_data_siswa()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		
		$filter = array(
			'KELAS_PARALEL'=>$KELAS_PARALEL,
			'PARALEL'=>$PARALEL
		);

		$data['r_data'] = $this->M_apps->check_data_result($filter,'tb_siswa_lengkap_aktif');
		$this->load->view('upload/v_filter_siswa',$data);
	}

	public function data()
	{
		$KELAS_PARALEL 	= $this->input->post('KELAS_PARALEL');
		$PARALEL 		= $this->input->post('PARALEL');
		$ID_SISWA 		= $this->input->post('ID_SISWA');
		$M_UPLOAD_ID 		= $this->input->post('M_UPLOAD_ID');

		if (!empty($M_UPLOAD_ID)) {
			$filter_upload = " AND M_UPLOAD_ID = ".$M_UPLOAD_ID;
		}else{
			$filter_upload="";
		}
		
		if (empty($ID_SISWA)) {
			$filter = "SELECT * FROM `v_upload_siswa` WHERE KELAS_PARALEL = '$KELAS_PARALEL' AND PARALEL = '$PARALEL' ".$filter_upload;		# code...
		}else{
			$filter = "SELECT * FROM `v_upload_siswa` WHERE KELAS_PARALEL = '$KELAS_PARALEL' AND PARALEL = '$PARALEL' AND ID_SISWA= '$ID_SISWA'" .$filter_upload;
		}


		$data['KELAS_PARALEL'] 	= $this->input->post('KELAS_PARALEL');
		$data['PARALEL'] 		= $this->input->post('PARALEL');
		$data['ID_SISWA'] 		= $this->input->post('ID_SISWA');		
		
		$data['r_data'] = $this->db->query($filter)->result();
		$this->load->view('upload/v_data',$data);
	}


	function view_upload(){
		$data['data_m_upload'] 	= $this->M_apps->tampil_data('m_upload');
		$data['konten'] 			= 'upload/view_list';
		$this->load->view('tampilan_backend',$data);
	}

	function form_add($id=''){
		$filter = array('ID_SISWA'=>$id);
		$data['data_t_upload'] = $this->M_apps->check_data_result($filter,'t_upload');
		$data['data_upload'] = $this->M_apps->tampil_data('m_upload');
		$data['data_siswa'] = $this->M_apps->edit_data($filter,'tb_siswa_lengkap_aktif');
		$data['id_siswa'] = $id; 
		$this->load->view('upload/upload_view',$data);
	}

	function lihat_data($id='',$id_siswa=''){
		$filter = array('T_UPLOAD_ID'=>$id);
		$data['data_t_upload'] = $this->M_apps->edit_data($filter,'t_upload');
		$data['data_upload'] = $this->M_apps->tampil_data('m_upload');
		$data['id_siswa'] = $id_siswa; 
		$this->load->view('upload/lihat_data',$data);
	}

	public function form_dataupload($id='')
	{
		$filter = array('ID_SISWA'=>$id);
		$data['data_t_upload'] = $this->M_apps->check_data_result($filter,'t_upload');
		$data['data_upload'] = $this->M_apps->tampil_data('m_upload');
		$data['data_siswa'] = $this->M_apps->edit_data($filter,'tb_siswa_lengkap_aktif');
		$data['id_siswa'] = $id; 
		$this->load->view('upload/form_dataupload',$data);
	}


	function do_upload(){
		
		$ID_SISWA= $this->input->post('ID_SISWA');
		$M_UPLOAD_ID = $this->input->post('M_UPLOAD_ID');
		$NO_INDUK = $this->input->post('NO_INDUK');
		$NAMA_SISWA =str_replace(" ", "_", $this->input->post('NAMA_SISWA'));

		$filter = array('M_UPLOAD_ID'=>$M_UPLOAD_ID);
        $data_m_upload = $this->M_apps->edit_data($filter,'m_upload');

        // CEK FILE UPLOAD
        $filter_upload = array('M_UPLOAD_ID'=>$M_UPLOAD_ID,'ID_SISWA'=>$ID_SISWA);
        $data_t_upload = $this->M_apps->edit_data($filter_upload,'t_upload');
        if(file_exists("./assets/upload/".$ID_SISWA."/".$data_t_upload->T_UPLOAD_NAMA)){
        	unlink("./assets/upload/".$ID_SISWA."/".$data_t_upload->T_UPLOAD_NAMA);
        }
		// buat Folder
		if (!file_exists("./assets/upload/".$ID_SISWA)) {
			mkdir("./assets/upload/".$ID_SISWA);
		}

		if(!empty($data_t_upload->T_UPLOAD_ID)){
			$status_form = "update_data";
		}else{
			$status_form = "input_data";
		}

		// UPLOAD GAMBAR
		$this->load->library('upload');
        $nmfile = $NO_INDUK."_".$NAMA_SISWA."_".$data_m_upload->M_UPLOAD_SINGKATAN; //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path']="./assets/upload/".$ID_SISWA; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

       	$this->upload->initialize($config);
       	
     //    $config['upload_path']="./assets/upload/".$ID_SISWA;
     //    $config['allowed_types']='gif|jpg|png';
     //    $config['encrypt_name'] = TRUE;
     //    $config['file_name'] = $ID_SISWA."_".$data_m_upload->M_UPLOAD_SINGKATAN; //nama yang terupload nantinya

     //    $this->load->library('upload',$config);
	    if($this->upload->do_upload("file")){
	        $data = $this->upload->data();

	    //   Resize and Compress Image
         //    $config['image_library']='gd2';
         //    $config['source_image']='./assets/upload/'.$data['file_name'];
         //    $config['create_thumb']= FALSE;
         //    $config['maintain_ratio']= FALSE;
         //    $config['quality']= '100%';
         //    $config['new_image']= './assets/upload/'.$data['file_name'];
         //    $this->load->library('image_lib', $config);
         //    $this->image_lib->resize();

	        $data_dokumen['ID_SISWA']= $this->input->post('ID_SISWA');
	        $data_dokumen['T_UPLOAD_KETERANGAN']= $this->input->post('T_UPLOAD_KETERANGAN');
	        $data_dokumen['M_UPLOAD_ID']= $this->input->post('M_UPLOAD_ID');
	        $data_dokumen['T_UPLOAD_NAMA'] = $data['file_name'];
	        $data_dokumen['T_UPLOAD_TGL'] = date('Y-m-d');
	        $filter_update = array('T_UPLOAD_ID'=>$data_t_upload->T_UPLOAD_ID);

	        if ($status_form=="update_data") {
	        	$result= $this->upload_model->update_data($filter_update,$data_dokumen,'t_upload');
	        }else{
	        	$result= $this->upload_model->save_upload($data_dokumen);
	        }
	        
	        echo json_decode(array("status"=>true,'pesan'=>'simpan'));
        }

     }

     function download_data($id='',$id_siswa=''){
		$filter = array('T_UPLOAD_ID'=>$id);
		$data = $this->M_apps->edit_data($filter,'t_upload'); 
		$file = 'assets/upload/'.$id_siswa."/".$data->T_UPLOAD_NAMA;
		// $data["file"] = file_get_contents("assets/uploads/".$id);
		echo $file;
		force_download($file,NULL);
	}

     
	
}