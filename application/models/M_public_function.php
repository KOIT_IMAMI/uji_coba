<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_public_function extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }
	
	function getYear_dd()
	{
		$year_now=date("Y");
		$y=array();
		for ($i=$year_now; $i >$year_now-40 ; $i--) { 
			$y[$i]=$i;
		}
		return $y;
	}
	
	public function get_date($value='')
	{
		if (empty($value)) {
			return "";
		}
		$nama_bulan = array(
			'01' => 'Januari', 
			'02' => 'Febuari', 
			'03' => 'Maret', 
			'04' => 'April', 
			'05' => 'Mei', 
			'06' => 'Juni', 
			'07' => 'Juli', 
			'08' => 'Agustus', 
			'09' => 'September', 
			'10' => 'Oktober', 
			'11' => 'November', 
			'12' => 'Desember', 
			);
		$v = date('d-m-Y',strtotime($value));
		$part_date=explode("-",$v);
		//$date = strftime( "%A, %d %B %Y %H%M", time());
		//Sabtu, 08 Agustus 2009 1842 
		//$date = strftime( "%d %B %Y", time());
		$result=$part_date[0].' '.$nama_bulan[$part_date[1]].' '.$part_date[2];
		return $result;
	}

	public function get_date_add_years($value='',$years)
	{
		if (empty($value)) {
			return "";
		}
		$nama_bulan = array(
			'01' => 'Januari', 
			'02' => 'Febuari', 
			'03' => 'Maret', 
			'04' => 'April', 
			'05' => 'Mei', 
			'06' => 'Juni', 
			'07' => 'Juli', 
			'08' => 'Agustus', 
			'09' => 'September', 
			'10' => 'Oktober', 
			'11' => 'November', 
			'12' => 'Desember', 
			);
		$v = date('d-m-Y',strtotime('+2 years', strtotime($value)));
		$part_date=explode("-",$v);
		//$date = strftime( "%A, %d %B %Y %H%M", time());
		//Sabtu, 08 Agustus 2009 1842 
		//$date = strftime( "%d %B %Y", time());
		$result=$part_date[0].' '.$nama_bulan[$part_date[1]].' '.$part_date[2];
		return $result;
	}
	public function get_date_now()
	{
		return $this->get_date(date("Y-m-d"));
	}

	public function get_day_now()
	{		
		return $this->convert_day(strftime( "%u", time()));
	}

	public function convert_day($value='')
	{
		$nama_hari = array(
			'1' => 'Senin', 
			'2' => 'Selasa', 
			'3' => 'Rabu', 
			'4' => 'Kamis', 
			'5' => 'Jumat', 
			'6' => 'Sabtu', 
			'7' => 'Minggu', 
			);
		return $nama_hari[$value];
	}

	public function age_calculator($dob){
		if(!empty($dob)){
			$birthdate = new DateTime($dob);
			$today   = new DateTime('today');
			$age = $birthdate->diff($today)->y;
			return $age;
		}else{
			return 0;
		}
	}

	public function recurse_copy($src,$dst) { 
		if (is_dir($src)) {

		    $dir = opendir($src); 
		    @mkdir($dst); 
		    while(false !== ( $file = readdir($dir)) ) { 
		        if (( $file != '.' ) && ( $file != '..' )) { 
		            if ( is_dir($src . '/' . $file) ) { 
		                $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
		            } 
		            else { 
		                copy($src . '/' . $file,$dst . '/' . $file); 
		            } 
		        } 
		    } 
		    closedir($dir); 
		}
	} 
	
	public function date_diff_year($start_date, $end_date)
	{
	   return floor((strtotime($end_date) - strtotime($start_date))/86400/365);
	}

	public function send_mail($to,$subject,$message)
    {
        $this->load->library('email');
        $this->email->from('your@example.com', 'Your Name');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }

}

/* End of file m_field.php */
/* Location: ./application/models/m_field.php */