<?php foreach ($data_upload as $r_data): ?>
              <?php  
              $filter = array('ID_SISWA'=>$id_siswa,'M_UPLOAD_ID'=>$r_data->M_UPLOAD_ID);
              $data_t_upload = $this->M_apps->edit_data($filter,'t_upload');
              ?>
                <form class="form-horizontal" id="submit<?php echo $r_data->M_UPLOAD_ID ?>">
                 <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label"><?php echo $r_data->M_UPLOAD_NAMA ?></label>
                      <div class="col-sm-4">
                        <input type="hidden" name="T_UPLOAD_KETERANGAN" value="<?php echo $r_data->M_UPLOAD_NAMA ?>">
                        <input type="hidden" name="NAMA_SISWA" value="<?php echo $data_siswa->NAMA_SISWA ?>">
                        <input type="hidden" name="ID_SISWA" value="<?php echo $id_siswa ?>">
                        <input type="hidden" name="M_UPLOAD_ID" value="<?php echo $r_data->M_UPLOAD_ID ?>">
                        <input type="hidden" name="NO_INDUK" value="<?php echo $data_siswa->NO_INDUK ?>">
                        <input type="hidden" name="ID_SISWA" value="<?php echo $id_siswa ?>">
                        <input type="file" class="form-control" name="file" placeholder="" onchange="upload_data('<?php echo $r_data->M_UPLOAD_ID ?>')">
                      </div>
                      <div class="col-sm-4">
                        <?php if (!empty($data_t_upload->T_UPLOAD_ID)): ?>
                          <button class="btn btn-warning" onclick="lihat_data('<?php echo $data_t_upload->T_UPLOAD_ID ?>','<?php echo $id_siswa ?>')"><i class="fa fa-search"></i> Lihat</button>
                          <span class="btn btn-success"><i class="icon fa fa-check"></i> Sukses Terupload</span>
                           <a class="btn btn-warning" href="<?php echo base_url() ?>Upload/download_data/<?php echo $data_t_upload->T_UPLOAD_ID ?>/<?php echo $id_siswa ?>"><i class="fa fa-download"></i></a>
                        <?php endif ?>
                        
                      </div>
                    </div>
                  </div>
                  
                </div>
                </form>
              <?php endforeach ?>
              <script type="text/javascript">
  $(document).ready(function(){
    <?php foreach ($data_upload as $r_data): ?>
    $('#submit<?php echo $r_data->M_UPLOAD_ID ?>').submit(function(e){
        e.preventDefault();
             $.ajax({
                 url:'<?php echo base_url();?>index.php/upload/do_upload',
                 type:"post",
                 data:new FormData(this), //this is formData
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
                  success: function(data){
                      $('#alert').html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> PEMBERITAHUAN!</h4>Upload Gambar Berhasil....</div>');
                      $('#form_uplaod').load("<?php echo base_url() ?>Upload/form_dataupload/<?php echo $id_siswa ?>");
                      
               }
             });s
    });
    <?php endforeach ?>
  });
</script>

<script type="text/javascript">
  function upload_data(id) {
    $('#submit'+id).submit();
  }
  function lihat_data(id_siswa,id) {
    event.preventDefault();
    window.open('<?php echo base_url() ?>Upload/lihat_data/'+id_siswa+'/'+id,'popUpWindow','width=800,left=0,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,directories=no,status=yes');
  }
</script>