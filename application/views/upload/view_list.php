<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DATA E-BERKAS DOKUMEN SISWA
        <small>MTsS NURUT TAQWA</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">DATA PRESENSI SISWA</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">MTsS NURUT TAQWA</span>
              <span class="info-box-number">DATA E-BERKAS DOKUMEN SISWA</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    DATA E-BERKAS DOKUMEN SISWA
                  </span>
            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
       
        </div>
        <div class="col-md-12">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">DATA E-BERKAS DOKUMEN SISWA</h3>

              <div class="box-tools pull-right">
               
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-body">
               <form class="form-horizontal" id="form" action="<?php echo base_url() ?>C_absensi_siswa/cetak" method="POST" target="_BLANK">
              <div class="box-body">
                <div class="col-sm-8">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">KELAS SISWA</label>
                  <div class="col-sm-4">
                   <select id="KELAS_PARALEL" name="KELAS_PARALEL" class="form-control select2" onchange="ambil_data_siswa()">
                     <option value="">Pilih Kelas</option>
                     
                     <option value="7">7</option>
                     <option value="8">8</option>
                     <option value="9">9</option>
                   </select>
                  </div>
                  <div class="col-sm-4">
                   <select id="PARALEL" name="PARALEL" class="form-control select2" onchange="ambil_data_siswa()">
                    <option value="">Pilih Paralel</option>
                   
                     <option value="1">A</option>
                     <option value="2">B</option>
                     <option value="3">C</option>
                     <option value="4">D</option>
                   </select>
                  </div>
                </div>

                <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">BERKAS SISWA</label>
                      <div class="col-sm-8">
                       <select id="M_UPLOAD_ID" name="M_UPLOAD_ID" class="form-control select2" >
                         <option value="">Semua Berkas</option>
                         <?php foreach ($data_m_upload as $r_data): ?>
                           <option value="<?php echo $r_data->M_UPLOAD_ID ?>"><?php echo $r_data->M_UPLOAD_NAMA ?></option>
                         <?php endforeach ?>
                       </select>
                      </div>
                    </div>



             
                <div id="nama_siswa">
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">NAMA SISWA (OPSIONAL)</label>
                      <div class="col-sm-8">
                       <select id="ID_SISWA" name="ID_SISWA" class="form-control select2" >
                         <option value="">Semua Siswa</option>
                       </select>
                      </div>
                    </div>
                </div>
               

                
                
              </div>
              
              
              </div>

              <!-- /.box-body -->
              <div class="box-footer pull-right" >
      
                <a onclick="btn_cari()" target="_BLANK" class="btn btn-warning"><i class="fa fa-search"></i> CARI DATA SISWA</a>
              </div>
              
              <!-- /.box-footer -->
            </form>
            </div>

          </div>
          
        </div>
        <div id="tampil_data">
          
        </div>
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">

    function ambil_data_siswa(){
      event.preventDefault();
      var KELAS_PARALEL = $('#KELAS_PARALEL').val();
      var PARALEL = $('#PARALEL').val();
      if(KELAS_PARALEL==""||PARALEL==""){
        $('#ID_SISWA').attr('disabled',true);
      }else{
        $('#ID_SISWA').attr('disabled',false);
        $('#nama_siswa').load('<?php echo base_url() ?>upload/ambil_data_siswa',{
          'KELAS_PARALEL':KELAS_PARALEL,
          'PARALEL':PARALEL
        });
      }
      
    }
  </script>
  <script type="text/javascript">
    function btn_cari() {
      event.preventDefault();
      var KELAS_PARALEL = $('#KELAS_PARALEL').val();
      var PARALEL = $('#PARALEL').val();
      var ID_SISWA = $('#ID_SISWA').val();
      var M_UPLOAD_ID = $('#M_UPLOAD_ID').val();
      if (KELAS_PARALEL==""||PARALEL=="") {
        swal('PEMBERITAHUAN','DATA FIELD / KOLOM HARUS TERISI SEMUA!','warning');
      }else{
        $('#tampil_data').load('<?php echo base_url() ?>upload/data',{
          'KELAS_PARALEL':KELAS_PARALEL,
          'PARALEL':PARALEL,
          'ID_SISWA':ID_SISWA,
          'M_UPLOAD_ID':M_UPLOAD_ID
        });
        
      }
      
      
    }
  </script>