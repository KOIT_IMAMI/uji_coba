<div class="col-md-12">
          <div class="box box-danger box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">DATA E-BERKAS DOKUMEN SISWA</h3>

              <div class="box-tools pull-right">
               
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">              
               <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>NO INDUK</th>
                    <th>NAMA</th>
                    <th>KELAS</th>
                    <th>BERKAS</th>
                    <th>PILIHAN</th>
                    
                  </tr>
                </thead>

                <tbody>
                  <?php $no=1; foreach ($r_data as $row_data): ?>
                  <?php
                  if ($row_data->PARALEL=="1") {
                      $PARALEL = "A";
                  }else if ($row_data->PARALEL=="2") {
                     $PARALEL = "B";
                  }else if ($row_data->PARALEL=="3") {
                      $PARALEL = "C";
                  }else{
                    $PARALEL = "D";
                  }
                  ?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $row_data->NO_INDUK ?></td>
                      <td><?php echo $row_data->NAMA_SISWA ?></td>
                      <td><?php echo $row_data->KELAS_PARALEL."-".$PARALEL ?></td>
                      <td><?php echo $row_data->T_UPLOAD_KETERANGAN ?></td>
                      <td>
                        <button class="btn btn-warning" onclick="lihat_data('<?php echo $row_data->T_UPLOAD_ID ?>','<?php echo $row_data->ID_SISWA ?>')"><i class="fa fa-search"></i> Lihat</button>
                          <a class="btn btn-success" href="<?php echo base_url() ?>Upload/download_data/<?php echo $row_data->T_UPLOAD_ID ?>/<?php echo $row_data->ID_SISWA ?>"><i class="fa fa-download"></i></a>
                      </td>
                      
                    </tr>
                  <?php endforeach ?>
                </tbody>
               
              </table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
    })
  })
</script>
<script type="text/javascript">
  function lihat_data(id_siswa,id) {
    event.preventDefault();
    window.open('<?php echo base_url() ?>Upload/lihat_data/'+id_siswa+'/'+id,'popUpWindow','width=800,height=600,left=0,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,directories=no,status=yes');
  }
</script>
